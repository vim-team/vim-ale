vim-ale (3.3.0-1) unstable; urgency=medium

  * New upstream release.
    - Replace previous patches with the latest post-release fixes.
  * Rewrite d/watch file for updated GitHub HTML code.
  * Bump standards version to 4.6.2, no related changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 09 Jan 2023 21:49:01 +0300

vim-ale (3.2.0-1) unstable; urgency=medium

  * New upstream release.
    - Apply also post-release fixes from Git.
  * New autopkgtest checks matching plugin and package versions.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 09 Jul 2022 19:26:03 +0300

vim-ale (3.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix ALE identification applying upstream's Bump-version.patch.
  * Fix executable-not-elf-or-script Lintian warning by debian/rules.
  * Inject Dh helper vim-addon by means of the debian/control file and the
    virtual package dh-sequence-vim-addon.
  * Bump Standards-Version to 4.6.0, no necessary changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 21 Feb 2022 21:15:55 +0300

vim-ale (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Mark all auto-tests as flaky to unblock migration to testing.
  * Override package-contains-documentation-outside-usr-share-doc Lintian tag.
  * Move Git repository to vim-team namespace on salsa.debian.org.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 28 Oct 2020 20:37:50 +0300

vim-ale (2.6.0-2) unstable; urgency=medium

  * Extract entire list of authors to copyright file.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 29 Jul 2020 20:25:54 +0300

vim-ale (2.6.0-1) unstable; urgency=low

  * Initial upload. (Closes: #960543)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 24 May 2020 13:00:07 +0300
