Source: vim-ale
Section: editors
Priority: optional
Maintainer: Nicholas Guriev <guriev-ns@ya.ru>
Build-Depends: debhelper-compat (= 13), dh-sequence-vim-addon
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/dense-analysis/ale
Vcs-Git: https://salsa.debian.org/vim-team/vim-ale.git
Vcs-Browser: https://salsa.debian.org/vim-team/vim-ale

Package: vim-ale
Architecture: all
Depends: ${vim-addon:Depends}, ${misc:Depends}
Description: Asynchronous Lint Engine for Vim 8 and NeoVim
 ALE (Asynchronous Lint Engine) is a plugin providing linting (syntax checking
 and semantic errors) in NeoVim 0.2.0+ and Vim 8 while you edit your text files,
 and acts as a Vim Language Server Protocol client.
 .
 ALE makes use of NeoVim and Vim 8 job control functions and timers to run
 linters on the contents of text buffers and return errors as text is changed in
 Vim. This allows for displaying warnings and errors in files being edited in
 Vim before files have been saved back to a filesystem. In other words, this
 plugin allows you to lint while you type.
 .
 After installing the package, you can put ‘packadd! ale’ to your vimrc file to
 activate the plugin.
