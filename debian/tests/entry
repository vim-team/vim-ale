#!/bin/bash

# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2020 Nicholas Guriev <guriev-ns@ya.ru>

set -e -u

what=$(basename "$0")
where=$(dirname "$0")

case "${what}" in
  with-vim)
    executor='vim.basic -e'
    ;;
  with-gvim)
    executor='xvfb-run -a vim.gtk3 --nofork -g'
    ;;
  with-neovim)
    executor='nvim -es'
    ;;
  *)
    echo >&2 'What editor are you going to test? Invoke this script via' \
      'a corresponding symlink.'
    exit 127
    ;;
esac

BASH_XTRACEFD=1
export VADER_OUTPUT_FILE=/dev/stdout
set -x
$executor -u "${where}/vimrc" "+Vader! ${*:-test/**/*.vader}"
